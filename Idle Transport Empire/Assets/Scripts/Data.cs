using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Data : MonoBehaviour
{

    public TMP_Text CurrentBalanceText;
    public TMP_Text CurrentPollutionText;
    public TMP_Text CurrentSocialOrderText;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        CurrentBalanceText.text = gamemanager.instance.CurrentBalance.ToString("C2");
        CurrentPollutionText.text = gamemanager.instance.CurrentPollution.ToString("P2");
        CurrentSocialOrderText.text = gamemanager.instance.CurrentSocialOrder.ToString("P2");
    }
}
