using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class Facility : MonoBehaviour
{
    //Public variables for Gameplay
    public float BaseFacilityCost;
    public float BaseFacilityProfit;
    public float BaseFacilityPollution;
    public float BaseFacilitySocialOrder;
    public float FacilityMultiplier;
    public bool FacilityUnlocked;
    public int FacilityTimerCut;
    public int FacilityCount;
    public bool AutomaticUnlocked;
    public float FacilityTimer;
    public float AutomationCost;
    public Button AutomationButton;
    public bool UpgradeUnlocked;
    public float UpgradeCost;
    public Button UpgradeButton;


    //private variables 
    float NextFacilityCost;
    float CurrentTimer = 0;
    bool StartTimer;


    // Start is called before the first frame update
    void Start()
    {
        StartTimer = false;
        SetNextFacilityCost(BaseFacilityCost);
        FacilityUnlocked = false;
    }

    public float GetCurrentTimer()
    {
        return CurrentTimer;
    }

    public void SetNextFacilityCost(float amt)
    {
        NextFacilityCost = amt;
    }
    public float GetNextFacilityCost()
    {
        return NextFacilityCost;
    }

    public bool GetStartTimer()
    {
        return StartTimer;
    }

    public float GetAutomationCost()
    {
        return AutomationCost;
    }

    public float GetUpgradeCost()
    {
        return UpgradeCost;
    }

    // Update is called once per frame
    void Update()
    {
        if (StartTimer)
        {
            CurrentTimer += Time.deltaTime;
            if (CurrentTimer > FacilityTimer)
            {
                if (!AutomaticUnlocked)
                    StartTimer = false;

                CurrentTimer = 0f;
                gamemanager.instance.AddToPollution(BaseFacilityPollution * FacilityCount);
                gamemanager.instance.AddToSocialOrder(BaseFacilitySocialOrder * FacilityCount);
                gamemanager.instance.AddToBalance(BaseFacilityProfit * FacilityCount);
            }
        }
    }


    public void BuyFacility()
    {
        FacilityCount += 1;

        float Amt = -NextFacilityCost;
        NextFacilityCost = (BaseFacilityCost * Mathf.Pow(FacilityMultiplier, FacilityCount));
        gamemanager.instance.AddToBalance(Amt);
        
        //achievement unlocked when selected number of facilities
        if (FacilityCount % FacilityTimerCut == 0)
            FacilityTimer /= 2;
    }

    public void OnStartTimer()
    {
        if (!StartTimer && FacilityCount > 0)
            StartTimer = true;

    }

    //Automation stuff
    public void UnlockAutomation()
    {
        if (AutomaticUnlocked)
            return;
        if (gamemanager.instance.CanBuy(AutomationCost))
        {
            AutomationButton.onClick.AddListener(UnlockAutomation);
            gamemanager.instance.AddToBalance(-AutomationCost);
            AutomaticUnlocked = true;
        }
    }

    public void StopAutomatic()
    {
        AutomaticUnlocked = false;
    }

    //Upgrades stuff
    public void UnlockUpgrade()
    {
        if (UpgradeUnlocked)
            return;
        if (gamemanager.instance.CanBuy(UpgradeCost))
        {
            UpgradeButton.onClick.AddListener(UnlockUpgrade);
            gamemanager.instance.AddToBalance(-UpgradeCost);
            UpgradeUnlocked = true;
            this.transform.GetComponent<UIFacility>().UpgradeUnlocked();
            BaseFacilityPollution /= 2;
        }
    }
}
