using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;


public class UIFacility : MonoBehaviour
{
    public TMP_Text FacilityCountText;
    public Slider ProgressSlider;
    public TMP_Text BuyButtonText;
    public Button BuyButton;

    public Facility facility;

    public Button AutomationButton;
    public TMP_Text AutomationButtonText;

    public Button UpgradeButton;
    public TMP_Text UpgradeButtonText;

    void OnEnable()
    {
        gamemanager.OnUpdateBalance += UpdateUI;
    }
    void OnDisable()
    {
        gamemanager.OnUpdateBalance -= UpdateUI;
    }

    public void UpgradeUnlocked()
    {
        TMP_Text ButtonText = UpgradeButton.transform.Find("UnlockUpgradeButtonText").GetComponent<TMP_Text>();
        ButtonText.text = "PURCHASED";
    }

    void Awake()
    {
        facility = transform.GetComponent<Facility>();
    }


    // Start is called before the first frame update
    void Start()
    {
        FacilityCountText.text = facility.FacilityCount.ToString();
        UpgradeButtonText.text = "Buy " + facility.GetUpgradeCost().ToString("C2");
    }

    // Update is called once per frame
    void Update()
    {

        ProgressSlider.value = facility.GetCurrentTimer() / facility.FacilityTimer;

        //Hide panel until you can afford it
        CanvasGroup cg = this.transform.GetComponent<CanvasGroup>();
        if (!facility.FacilityUnlocked && !gamemanager.instance.CanBuy(facility.GetNextFacilityCost()))
        {
            cg.interactable = false;
            cg.alpha = 0;
        }
        else
        {
            cg.interactable = true;
            cg.alpha = 1;
            facility.FacilityUnlocked = true;
        }

        //Update button if you can afford the next facility
        if (gamemanager.instance.CanBuy(facility.GetNextFacilityCost()))
            BuyButton.interactable = true;
        else
            BuyButton.interactable = false;
        BuyButtonText.text = "Buy " + facility.GetNextFacilityCost().ToString("C2");
    }

    public void UpdateUI()
    {
        //Update Automation button if can afford it
        if (gamemanager.instance.CanBuy(facility.AutomationCost))
            AutomationButton.interactable = true;
        else
            AutomationButton.interactable = false;
        AutomationButtonText.text = "Buy " + facility.GetAutomationCost().ToString("C2");

        //Update Upgrade button if can afford it
        if (!facility.UpgradeUnlocked && gamemanager.instance.CanBuy(facility.UpgradeCost))
            UpgradeButton.interactable = true;
        else
            UpgradeButton.interactable = false;
        
    }

    public void BuyFacilityOnClick()
    {
        if (!gamemanager.instance.CanBuy(facility.GetNextFacilityCost()))
            return;
        facility.BuyFacility();
        FacilityCountText.text = facility.FacilityCount.ToString();
        UpdateUI();
    }

    public void OnTimerClick()
    {
        facility.OnStartTimer();
    }
}
