using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;


public class UIManager : MonoBehaviour
{
    public enum State
    {
        Main,Headquarter,Upgrades
    }
    public State CurrentState;
    public GameObject HeadquarterPanel;
    public GameObject UpgradesPanel;

    public TMP_Text CurrentBalanceText;
    public TMP_Text CurrentPollutionText;
    public TMP_Text CurrentSocialOrderText;

    // Start is called before the first frame update
    void Start()
    {
        CurrentState = State.Main;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnEnable()
    {
        gamemanager.OnUpdateBalance += UpdateUI;
    }
    void OnDisable()
    {
        gamemanager.OnUpdateBalance -= UpdateUI;
    }
    public void UpdateUI()
    {
        CurrentBalanceText.text = gamemanager.instance.CurrentBalance.ToString("C2");
        CurrentPollutionText.text = gamemanager.instance.CurrentPollution.ToString("P2");
        CurrentSocialOrderText.text = gamemanager.instance.CurrentSocialOrder.ToString("P2");
    }

    // State Machine set-Up
    void onShowHeadquarter()
    {
        CurrentState = State.Headquarter;
        HeadquarterPanel.SetActive(true);
    }

    void onShowUpgrades()
    {
        CurrentState = State.Upgrades;
        UpgradesPanel.SetActive(true);
    }

    void onSHowMain()
    {
        CurrentState = State.Main;
        HeadquarterPanel.SetActive(false);
        UpgradesPanel.SetActive(false);
    }

    public void onClickHeadquarter()
    {
        if (CurrentState == State.Main)
            onShowHeadquarter();
        else
            onSHowMain();
    }

    public void onClickUpgrades()
    {
        if (CurrentState == State.Main)
            onShowUpgrades();
        else
            onSHowMain();
    }

    public void OnClickCheat()
    {
        gamemanager.instance.AddToBalance(1000);
    }
}
