using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gamemanager : MonoBehaviour
{
    public delegate void UpdateBalance();
    public static event UpdateBalance OnUpdateBalance;

    public static gamemanager instance;

    public float CurrentBalance;
    public float CurrentPollution;
    public float CurrentSocialOrder;


    // Start is called before the first frame update
    void Start()
    {
        if (OnUpdateBalance != null)
            OnUpdateBalance();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
            Application.Quit();
    }

    void Awake()
    {
        if (instance == null)
            instance = this;

    }
    public void AddToBalance(float amt)
    {
        CurrentBalance += amt;
        if (OnUpdateBalance != null)
            OnUpdateBalance();
    }

    public void AddToPollution(float amt)
    {
        CurrentPollution += amt;
        if (OnUpdateBalance != null)
            OnUpdateBalance();
    }

    public void AddToSocialOrder(float amt)
    {
        CurrentSocialOrder += amt;
        if (OnUpdateBalance != null)
            OnUpdateBalance();
    }

    public bool CanBuy(float AmtToSpend)
    {
        if (AmtToSpend > CurrentBalance)
            return false;
        else
            return true;
    }
}
